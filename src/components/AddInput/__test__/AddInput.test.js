import { render, screen, fireEvent } from '@testing-library/react';
import AddInputTodo from '../AddInput';

const mockedSetTodo = jest.fn();

describe('AddInput', () => {
  it('should render Input elemet', async () => {
    render(<AddInputTodo todos={[]} setTodos={mockedSetTodo} />);
    const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
    expect(inputElement).toBeInTheDocument();
  });

  it('should be able to type into input', async () => {
    render(<AddInputTodo todos={[]} setTodos={mockedSetTodo} />);
    const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
    fireEvent.change(inputElement, {
      target: {
        value: 'Go Grocery shopping',
      },
    });
    expect(inputElement.value).toBe('Go Grocery shopping');
  });

  it('should have empty input when add button is clicked', async () => {
    render(<AddInputTodo todos={[]} setTodos={mockedSetTodo} />);
    const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
    const buttonElement = screen.getByRole('button', { name: /Add/i });
    fireEvent.change(inputElement, {
      target: {
        value: 'Go Grocery shopping',
      },
    });

    expect(inputElement.value).toBe('Go Grocery shopping');

    fireEvent.click(buttonElement);
    expect(inputElement.value).toBe('');
  });
});
