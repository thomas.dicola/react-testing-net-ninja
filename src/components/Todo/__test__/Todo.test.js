import { fireEvent, getByText, render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Todo from '../Todo';

const MockTodo = () => {
  return (
    <BrowserRouter>
      <Todo />
    </BrowserRouter>
  );
};

const addTask = (tasks) => {
  const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
  const buttonElement = screen.getByRole('button', { name: /Add/i });
  tasks.forEach((task) => {
    fireEvent.change(inputElement, { target: { value: task } });
    fireEvent.click(buttonElement);
  });
};

describe('Todo', () => {
  it('should be able to type into input', () => {
    render(<MockTodo />);
    addTask(['Go Grocery Shopping']);
    const divElement = screen.getByText(/Go Grocery Shopping/i);
    expect(divElement).toBeInTheDocument();
  });

  it('should render multiple items', () => {
    render(<MockTodo />);
    addTask([
      'Go Grocery Shopping',
      'Go Grocery Shopping',
      'Go Grocery Shopping',
    ]);
    const divElements = screen.queryAllByText(/Go Grocery Shopping/i);
    expect(divElements.length).toBe(3);
  });

  it('should render multiple items', async () => {
    render(<MockTodo />);
    addTask([
      'Go Grocery Shopping',
      'Go Grocery Shopping',
      'Go Grocery Shopping',
    ]);
    const divElement = screen.getAllByTestId('task-container');
    expect(divElement.length).toBe(3);
  });

  it('task should not have completed class when initally rendered', async () => {
    render(<MockTodo />);
    addTask(['Go Grocery Shopping']);
    const divElement = screen.getByText(/Go Grocery Shopping/i);
    expect(divElement).not.toHaveClass('todo-item-active');
  });
});
